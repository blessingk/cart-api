<?php
namespace App\Services;
use App\SocialFacebookAccount;
use App\Models\Customer;
use Laravel\Socialite\Contracts\User as ProviderUser;
class SocialFacebookAccountService
{
    public function createOrGetUser(ProviderUser $providerUser)
    {
        $account = SocialFacebookAccount::whereProvider('facebook')
            ->whereProviderUserId($providerUser->getId())
            ->first();
        if ($account) {
            return $account->customer;
        } else {
            $account = new SocialFacebookAccount([
                'provider_customer_id' => $providerUser->getId(),
                'provider' => 'facebook'
            ]);
            $customer = Customer::whereEmail($providerUser->getEmail())->first();
            if (!$customer) {
                $customer = Customer::create([
                    'email' => $providerUser->getEmail(),
                    'name' => $providerUser->getName(),
                    'password' => md5(rand(1,10000)),
                ]);
            }
            $account->customer()->associate($customer);
            $account->save();
            return $customer;
        }
    }
}
