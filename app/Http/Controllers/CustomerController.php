<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateCustomerRequest;
use App\Models\Customer;
use Illuminate\Http\Request;
use JWTAuth;
use Auth;
use Carbon\Carbon;
use App\Models\Review;

/**
 * Customer controller handles all requests that has to do with customer
 * Some methods needs to be implemented from scratch while others may contain one or two bugs.
 *
 *  NB: Check the BACKEND CHALLENGE TEMPLATE DOCUMENTATION in the readme of this repository to see our recommended
 *  endpoints, request body/param, and response object for each of these method
 *
 * Class CustomerController
 * @package App\Http\Controllers
 */
class CustomerController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt.auth' , ['except' => ['login', 'create']]);
    }
    /**
     * Allow customers to create a new account.
     *
     * @param CreateCustomerRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(CreateCustomerRequest $request)
    {
        $customer = new Customer();
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->password =  bcrypt($request->password);
        $customer->save();

        $customer =  Customer::find($customer->customer_id);
        // create token based on customer information
        $token = JWTAuth::fromUser($customer);
        // all good so return the token and user instance
        return response()->json([
            'customer' => $customer,
            'accessToken' => $token,
            'expiresIn' => $this->guard()->factory()->getTTL() * 60
        ], 200);
    }

    /**
     * Allow customers to login to their account.
     * Get a JWT token via given credentials.
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        try {
            // attempt to verify the credentials and create a token for the user
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Username or password is invalid.'], 401);
            }

        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // all good so return the token
        return response()->json([
            'customer' => Auth::user(),
            'accessToken' => $token,
            'expiresIn' => $this->guard()->factory()->getTTL() * 60
        ], 200);
    }

    /**
     * Allow customers to view their profile info.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCustomerProfile()
    {
        return response()->json(Auth::user(), 200);
    }

    /**
     * Allow customers to update their profile info like name, email, password, day_phone, eve_phone and mob_phone.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCustomerProfile(Request $request)
    {
        $customer = Auth::user();
        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->day_phone = $request->day_phone;
        $customer->eve_phone = $request->eve_phone;
        $customer->mob_phone = $request->mob_phone;
        $customer->save();
        return response()->json($customer, 200);
    }

    /**
     * Allow customers to update their address info/
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCustomerAddress(Request $request)
    {
        $customer = Auth::user();
        $customer->address_1 = $request->address_1;
        $customer->address_2 = $request->address_2;
        $customer->city = $request->city;
        $customer->postal_code = $request->postal_code;
        $customer->shipping_region_id = $request->shipping_region_id;
        $customer->save();
        return response()->json($customer, 200);
    }

    /**
     * Allow customers to update their credit card number.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCreditCard(Request $request)
    {
        $customer = Auth::user();
        $customer->credit_card = $request->credit_card;
        $customer->save();
        return response()->json($customer, 200);
    }

    /**
     * Apply something to customer.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function apply()
    {
        return response()->json(['message' => 'this works']);
    }

    public function guard()
    {
        return Auth::guard();
    }

    /**
     * Creates a customer review
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postProductReview(Request $request, $product_id)
    {
      $customer = Auth::user();
      $review = new Review();
      $review->product_id = $product_id;
      $review->customer_id = $customer->customer_id;
      $review->review = $request->review;
      $review->rating = $request->rating;
      $review->created_on = Carbon::now();
      $review->save();

      $review = Review::where('review_id', $review->review_id)
                    ->join('customer', 'customer.customer_id', '=', 'review.customer_id')
                    ->select('customer.name', 'review.review', 'review.rating', 'review.created_on')
                    ->first();

      return response()->json($review);

    }


}
