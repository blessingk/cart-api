<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Attribute;
use App\Models\AttributeValue;
use App\Models\ProductAttribute;

/**
 * The controller defined below is the attribute controller.
 * Some methods needs to be implemented from scratch while others may contain one or two bugs.
 *
 * NB: Check the BACKEND CHALLENGE TEMPLATE DOCUMENTATION in the readme of this repository to see our recommended
 *  endpoints, request body/param, and response object for each of these method
 *
 *
 * Class AttributeController
 * @package App\Http\Controllers
 */
class AttributeController extends Controller
{
    /**
     * This method should return an array of all attributes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllAttributes()
    {
        return response()->json(Attribute::all(), 200);
    }

    /**
     * This method should return a single attribute using the attribute_id in the request parameter.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSingleAttribute($attribute_id)
    {
        return response()->json(Attribute::find($attribute_id), 200);
    }

    /**
     * This method should return an array of all attribute values of a single attribute using the attribute id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAttributeValues($attribute_id)
    {
        return response()->json(AttributeValue::select('attribute_value_id', 'value')->where('attribute_id', $attribute_id)->get(), 200);
    }

    /**
     * This method should return an array of all the product attributes.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductAttributes($product_id)
    {
      $product_attribute_values = ProductAttribute::select('attribute.name as attribute_name', 'attribute_value.attribute_value_id', 'attribute_value.value as attribute_value')
                                        ->join('attribute_value', 'attribute_value.attribute_value_id', '=', 'product_attribute.attribute_value_id')
                                        ->join('attribute', 'attribute.attribute_id', '=', 'attribute_value.attribute_id')
                                        ->where('product_attribute.product_id', $product_id)
                                        ->get();
        return response()->json($product_attribute_values);

    }
}
