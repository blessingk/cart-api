<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\ShoppingCart;
use DB;


/**
 * Check each method in the shopping cart controller and add code to implement
 * the functionality or fix any bug.
 *
 *  NB: Check the BACKEND CHALLENGE TEMPLATE DOCUMENTATION in the readme of this repository to see our recommended
 *  endpoints, request body/param, and response object for each of these method
 *
 * Class ShoppingCartController
 * @package App\Http\Controllers
 */
class ShoppingCartController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt.auth');
    }
    /**
     * To generate a unique cart id.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function generateUniqueCart()
    {
        return response()->json(ShoppingCart::getOrCreateCartId(Auth::user()));
    }

    /**
     * To add new product to the cart.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function addItemToCart(Request $request)
    {
        $cart = ShoppingCart::where(['product_id' => $request->product_id, 'cart_id' => Auth::user()->customer_id])->first();
        if(!$cart){
          $cart = new ShoppingCart();
        }
        $cart->cart_id = $request->cart_id;
        $cart->product_id = $request->product_id;
        $cart->attributes = $request->get('attributes');
        $cart->quantity += $request->quantity;
        $cart->added_on = Carbon::now();
        $cart->customer_id = Auth::user()->customer_id;
        $cart->save();

        $cart = ShoppingCart::where('item_id', $cart->item_id)
                          ->select('item_id', 'cart_id', 'attributes', 'product_id', 'quantity')
                          ->first();

        return response()->json($cart);
    }

    /**
     * Method to get list of items in a cart.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCart($cart_id)
    {
        $cart = ShoppingCart::where('shopping_cart.cart_id', $cart_id)
                            ->select('shopping_cart.item_id', 'shopping_cart.cart_id', 'shopping_cart.attributes', 'product.product_id',
                            'shopping_cart.quantity', 'product.image', 'product.price', 'product.discounted_price', 'product.name',
                            DB::raw('(product.price * shopping_cart.quantity) as subtotal'))
                            ->join('product', 'product.product_id', '=', 'shopping_cart.product_id')
                            ->get();

        return response()->json($cart);
    }

    /**
     * Update the quantity of a product in the shopping cart.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateCartItem(Request $request, $item_id)
    {
        $cart_item = ShoppingCart::where('item_id', $item_id)
                          ->select('item_id', 'cart_id', 'attributes', 'product_id', 'quantity')
                          ->first();
        $cart_item->quantity = $request->quantity;
        $cart_item->save();

        return response()->json($cart_item);
    }

    /**
     * Should be able to clear shopping cart.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function emptyCart($cart_id)
    {
        ShoppingCart::where('cart_id', $cart_id)->delete();
        return response()->json();
    }

    /**
     * Should delete a product from the shopping cart.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function removeItemFromCart($item_id)
    {
        ShoppingCart::where('item_id', $item_id)->delete();
        return response()->json(['message' => 'Item removed!']);
    }

    /**
     * Create an order.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createOrder(Request $request)
    {
        $customer = Auth::user();
        $order = new Order();
        // $order->cart_id = $request->cart_id;
        $order->shipping_id = $request->shipping_id;
        $order->tax_id =  $request->tax_id;
        $order->customer_id =  $customer->customer_id;
        $order->created_on =  Carbon::now();
        $order->save();
        return response()->json($order->order_id);
    }

    /**
     * Get all orders of a customer.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCustomerOrders()
    {
        $orders = Order::where('customer.customer_id', Auth::user()->customer_id)
                        ->select('orders.order_id', 'orders.total_amount', 'orders.created_on', 'orders.shipped_on', 'customer.name')
                        ->join('customer', 'customer.customer_id', '=', 'orders.customer_id')
                        ->get();
        return response()->json($orders);
    }

    /**
     * Get the details of an order.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderSummary($order_id)
    {
        $order_items = OrderDetail::where('order_id', $order_id)->get();
        return response()->json(['order_id' => $order_id, 'order_items' => $order_items]);
    }

    /**
     * Get the details of an order.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderShortDetails($order_id)
    {
        $orders = Order::where('orders.order_id', $order_id)
                      ->select('orders.order_id', 'orders.total_amount', 'orders.created_on', 'orders.shipped_on', 'customer.name', 'orders.status')
                      ->join('customer', 'customer.customer_id', '=', 'orders.customer_id')
                      ->get();
        return response()->json($orders);
    }

    /**
     * Process stripe payment.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function processStripePayment()
    {
        return response()->json(['message' => 'this works']);
    }
}
