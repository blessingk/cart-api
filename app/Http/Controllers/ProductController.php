<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Department;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\Category;
use Searchy;
use App\Models\Review;
use Auth;

/**
 * The Product controller contains all methods that handles product request
 * Some methods work fine, some needs to be implemented from scratch while others may contain one or two bugs/
 *
 *  NB: Check the BACKEND CHALLENGE TEMPLATE DOCUMENTATION in the readme of this repository to see our recommended
 *  endpoints, request body/param, and response object for each of these method.
 */
class ProductController extends Controller
{

  public function __construct(){
      $this->middleware('guest', ['except', ['postProductReview']]);
  }

    /**
     * Return a paginated list of products.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllProducts(Request $request)
    {
        $products = Product::select("product.product_id", "product.name", "product.description",
                                    "product.price", "product.discounted_price", "product.thumbnail")
                                   ->get();
//        $paginationMeta = json_encode([
//            "currentPage" => $request->currentPage,
//            "currentPageSize" => $request->currentPageSize,
//            "totalPages" => $products->count()/$request->currentPageSize,
//            "totalRecords" => $products->count(),
//          ]);

        return response()->json(['count' => $products->count(), 'rows' => $products ], 200);
    }

    /**
     * Returns a single product with a matched id in the request params.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProduct($product_id)
    {
        return response()->json(Product::find($product_id), 200);
    }

    /**
     * Returns details of a product.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductDetails($product_id)
    {
        return response()->json([Product::find($product_id)], 200);
    }

    /**
     * Returns product locations.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductLocations($product_id)
    {
        $product_categories = ProductCategory::select('category.category_id', 'category.name as category_name', 'department.department_id', 'department.name as department_name')
                                          ->join('category', 'category.category_id', '=', 'product_category.category_id')
                                          ->join('department', 'department.department_id', '=', 'category.department_id')
                                          ->where('product_id', $product_id)
                                          ->get();
        return response()->json($product_categories, 200);
    }

    /**
     * Returns a list of product that matches the search query string.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchProduct(Request $request, Product $products)
    {
//        return response()->json(['test', $request->all()]);
//        $products = Searchy::product("name")
//                            ->select("product_id", "name", "description", "price", "discounted_price", "thumbnail")
//                            ->query($request->keyword)
//                            ->getQuery()
////                            ->limit($request->limit)
//                            ->get();

        $query = $products->newQuery();

        //  filter by keyword
        if($request->has('keyword') && isset($request->keyword)) {
            $query->where('name', 'LIKE' , '%' . $request->keyword . '%');
//            ->orWhere('description', 'LIKE' , '%' . $request->keyword . '%');
        }

        //filter by price range
        if($request->has('min_price') && $request->min_price != 0){
            $query->where('price', '>=', $request->min_price);
        }

        //filter by price range
        if($request->has('max_price') && $request->max_price != 0){
            $query->where('price', '<=', $request->max_price);
        }

        $products = $query->get();
        return response()->json(['count' => $products->count(), 'rows' => $products]);
    }

    /**
     * Returns all products in a product category.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductsByCategory($categoty_id)
    {
        $products = ProductCategory::select("product.product_id", "product.name", "product.description",
                                          "product.price", "product.discounted_price", "product.thumbnail")
                                          ->join('product', 'product.product_id', '=', 'product_category.product_id')
                                          ->where('category_id', $categoty_id)
                                          ->get();
        return response()->json(['count' => $products->count(), 'rows' => $products], 200);
    }

    /**
     * Returns a the category of a particular product.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductCategory($product_id)
    {
        $category = ProductCategory::select('category.category_id', 'category.department_id', 'category.name')
                                          ->join('category', 'category.category_id', '=', 'product_category.category_id')
                                          ->where('product_id', $product_id)
                                          ->first();
        return response()->json($category, 200);
    }

    /**
     * Returns a list of products in a particular department.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductsInDepartment($department_id)
    {
        $products = ProductCategory::select("product.product_id", "product.name", "product.description",
                                        "product.price", "product.discounted_price", "product.thumbnail")
                                        ->join('product', 'product.product_id', '=', 'product_category.product_id')
                                        ->join('category', 'category.category_id', '=', 'product_category.category_id')
                                        ->where('category.department_id', $department_id)
                                        ->get();
        return response()->json(['count' => $products->count(), 'rows' => $products]);
    }

    /**
     * Returns a list of all product departments.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllDepartments()
    {
        return response()->json(Department::all(), 200);
    }

    /**
     * Returns a single department.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDepartment($department_id)
    {
        return response()->json(Department::findOrFail($department_id), 200);

    }

    /**
     * Returns all categories.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getAllCategories()
    {
        return response()->json(['count' => Category::count(), 'rows' => Category::all()], 200);
    }

    /**
     * Returns a single category.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategory($category_id)
    {
        return response()->json(Category::findOrFail($category_id), 200);
    }

    /**
     * Returns all categories in a department.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getDepartmentCategories($department_id)
    {
        return response()->json(Category::where('department_id', $department_id)->get(), 200);
    }

    /**
     * Returns a list reviews for a product
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductReviews($product_id)
    {
        $reviews = Review::where('product_id', $product_id)
                      ->join('customer', 'customer.customer_id', '=', 'review.customer_id')
                      ->select('customer.name', 'review.review', 'review.rating', 'review.created_on')
                      ->get();
        return response()->json($reviews, 200);
    }
}
