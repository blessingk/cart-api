<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Socialite;
use App\Services\SocialFacebookAccountService;
use JWTAuth;
use Auth;

class SocialAuthFacebookController extends Controller
{
  /**
 * Create a redirect method to facebook api.
 *
 * @return void
 */
  public function facebookLogin()
  {
      return Socialite::driver('facebook')->redirect();
  }
  /**
   * Return a callback method from facebook api.
   *
   * @return callback URL from facebook
   */
  public function callback(SocialFacebookAccountService $service)
  {
      $customer = $service->createOrGetUser(Socialite::driver('facebook')->user());
      $token = JWTAuth::fromUser($customer);
      // all good so return the token and user instance
      return response()->json([
          'customer' => $customer,
          'accessToken' => $token,
          'expiresIn' => Auth::guard()->factory()->getTTL() * 60
      ], 200);
  }
}
