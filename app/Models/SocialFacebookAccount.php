<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Customer;

class SocialFacebookAccount extends Model
{
    protected $timestamps = false;
    protected $fillable = ['customer_id', 'provider_customer_id', 'provider'];

    public function customer()
    {
        return $this->belongsTo(Customer::class);
    }
}
